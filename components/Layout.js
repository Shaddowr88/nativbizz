import React, {Component} from 'react';
import {SafeAreaView} from 'react-native';
import Head from './Head';
import CardPresentation from './CardPresentation';

class Layout extends Component {
  render() {
    return (
      <SafeAreaView>
        <Head />
        <CardPresentation />
      </SafeAreaView>
    );
  }
}
export default Layout;
