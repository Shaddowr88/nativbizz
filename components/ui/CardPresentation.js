import React from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';

export default class CardPresentation extends React.Component {
  render() {
    return (
      <View style={[Style.sectionContainer, Style.shadowProp]}>
        <ImageBackground
          source={require('../assets/img/44d85efbbe77f42aa56599a6a87b6de8.jpg')}
          resizeMode="cover"
          style={Style.image}>
          <View>
            <Text style={Style.infoTitle}>îlot I</Text>
            <Text style={Style.infoTitle}>La coudraie</Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 20}}>
            <View style={Style.selectItem2}>
              <Text>Gestionnaire Syndic</Text>
            </View>
            <Text style={{color: 'white'}}>Aqedendisdjfbf</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <View style={Style.selectItem2}>
              <Text>AG prévus</Text>
            </View>
            <Text style={{color: 'white', fontSize: 20, fontWeight: 'bold'}}>
              88 SEPT 20
            </Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <View style={Style.selectItem2}>
              <Text>Budget</Text>
            </View>
            <Text
              style={{
                color: 'white',
                fontSize: 25,
                fontWeight: 'bold',

              }}>
              888888
            </Text>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const Style = StyleSheet.create({
  sectionContainer: {
    paddingHorizontal: 30,
    flexDirection: 'row',
    alignItems: 'center',
  },
  selectItem2: {
    flex: 1,
    height: 20,
    minWidth: '50%',
    borderRadius: 100,
    backgroundColor: 'white',
    alignItems: 'center',
    padding: 0.5,
    marginBottom: 8,
    marginEnd: 45,
  },
  image: {
    flex: 1,
    justifyContent: 'flex-end',
    height: 550,
    width: 330,
    marginTop: 30,
    padding: 10,
    borderRadius: 10,
    //borderStyle: 'solid',
    //borderWidth: 1,
    overflow: 'hidden',
  },
  shadowProp: {
    shadowColor: '#171717',
    shadowOffset: {width: -3, height: 4},
    shadowOpacity: 0.4,
    shadowRadius: 4,
  },
  infoTitle: {
    color: 'white',
    fontSize: 50,
    maxHeight: 100,
    lineHeight: 50,
  },
  Zone: {
    paddingBottom: 2,
  },
});
