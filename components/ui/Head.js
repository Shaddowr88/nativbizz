import React, {Component} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';

export default class Head extends React.Component {
  render() {
    return (
      <View style={Style.globalContainer}>
        <View style={Style.selectedContainer}>
          <Image
            style={Style.userPicture}
            source={require('../assets/img/006272604a139bb074f015272e0cfd18.jpg')}
          />
        </View>
        <View style={Style.selectedContainer2}>
          <Text>NataNaël</Text>
          <Text>Aqedendi torpescit</Text>
        </View>
        <View style={[Style.selectItem, Style.shadowProp]}>
          <Text>•••</Text>
        </View>
      </View>
    );
  }
}

const Style = StyleSheet.create({
  sectionContainer: {},
  globalContainer: {
    height: '10%',
    paddingHorizontal: 30,
    flexDirection: 'row',
    alignItems: 'center',
  },
  userPicture: {
    height: 70,
    width: 70,
    borderRadius: 100,
  },
  selectedContainer2: {
    flex: 0.8,
  },
  selectedContainer: {
    flex: 0.45,
  },
  selectItem: {
    flex: 0.2,
    height: 40,
    width: 50,
    borderRadius: 100,
    backgroundColor: 'white',
    alignItems: 'center',
    paddingTop: 10,
    marginLeft: 50,
  },
  shadowProp: {
    shadowColor: '#171717',
    shadowOffset: {width: -1, height: 2},
    shadowOpacity: 0.2,
    shadowRadius: 3,
  },
});
